package com.cognizant.iot.completeanalytics;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.cognizant.iot.completeanalytics.StoreViewOther.MainStoreActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;

public class MainActivity extends AppCompatActivity {

    Button store,org;
    public static JSONObject tray1,tray2,tray3;

    public static String date="2016-01-01";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        store=(Button)findViewById(R.id.store);
        org=(Button)findViewById(R.id.org);
        try
        {
            tray1=new JSONObject(loadJSONFromAsset("Tray1.json"));
            tray2=new JSONObject(loadJSONFromAsset("Tray2.json"));
            tray3=new JSONObject(loadJSONFromAsset("Tray3.json"));
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
        store.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, MainStoreActivity.class);
                startActivity(intent);

            }
        });
        org.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this,OrganizationActivity.class);
                startActivity(intent);
            }
        });
    }

    private String loadJSONFromAsset(String name) {
        String json = null;
        try {
            InputStream is = getApplicationContext().getAssets().open(name);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        Log.e("JSON",json);
        return json;
    }
}
