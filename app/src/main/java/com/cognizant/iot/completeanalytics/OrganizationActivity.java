package com.cognizant.iot.completeanalytics;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.cognizant.iot.completeanalytics.Model.DeviceLineModel;
import com.cognizant.iot.completeanalytics.Model.GsonModel;
import com.cognizant.iot.completeanalytics.Model.SmartTrayColors;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.StringTokenizer;

public class OrganizationActivity extends AppCompatActivity {

    public static LineChart orgchart;
    JSONObject store1avg,store2avg,store3avg;
    ArrayList<LineDataSet> alllines;
    ArrayList<String> line_labels;
    ArrayList<Entry> storeentry;
    RecyclerView yearholder;
    public static RecyclerView monthholder;
    public static RecyclerView weekholder;
    public static Spinner spinner;
    Date date;
    public static String datetoshow,dayofweek;
    TextView selecteddate;
    LinearLayout datelayout;
    public static TextView yeartoshow;
    public static ImageView before,after;
    final String[] spinneritems={"Year","Month","Week",""};
    public static int year;
    public static int monthno,weeknumber;
    public static ArrayList<String> month;
    public static ArrayList<String> week;
    public static ArrayList<String> day;
    Boolean yearfirsttime=true,monthfirsttime=true,weekfirsttime=true,dayfirsttime=true;
    String selectedinspinner;
    int color_no=0;
    int pos;
    public static ViewFlipper viewFlipper;
    public static ViewFlipper monthFlipper;
    ArrayList<String> monthlabels;
    ArrayList<Entry> store1entry;
    ArrayList<Entry> store2entry;
    public static OrgMonthAdapter monthAdapter;
    public static ImageView monthbefore,monthnext,weekbefore,weeknext,daybefore,daynext;
    public static TextView selectedmonthinflipper,selecteddayinflipper;
    public static TextView weekno;
    public static OrgDayAdapter dayAdapter;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_organization);
        orgchart=(LineChart) findViewById(R.id.orgchart);
        viewFlipper=(ViewFlipper) findViewById(R.id.viewflipper);
        monthFlipper=(ViewFlipper) findViewById(R.id.textflipper);
        monthFlipper.setDisplayedChild(0);
        monthbefore=(ImageView) findViewById(R.id.monthbefore);
        monthnext=(ImageView) findViewById(R.id.monthnext);
        weekbefore=(ImageView) findViewById(R.id.weekbefore);
        weeknext=(ImageView) findViewById(R.id.weeknext);
        daybefore=(ImageView) findViewById(R.id.daybefore);
        daynext=(ImageView) findViewById(R.id.daynext);
        selectedmonthinflipper=(TextView) findViewById(R.id.month);
        selecteddayinflipper=(TextView) findViewById(R.id.day);
        weekno=(TextView) findViewById(R.id.week);
        store1avg=MainActivity.tray1;
        store2avg=MainActivity.tray2;
        store3avg=MainActivity.tray3;
        datelayout=(LinearLayout) findViewById(R.id.datelayout);
        datelayout.setGravity(Gravity.BOTTOM);
        yeartoshow=(TextView)findViewById(R.id.year);
        before=(ImageView) findViewById(R.id.before);
        after=(ImageView) findViewById(R.id.next);
        date=new Date();
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        SimpleDateFormat outFormat = new SimpleDateFormat("EEEE");
        dayofweek = outFormat.format(date);
        Log.e("day",dayofweek);
        SimpleDateFormat sdf=new SimpleDateFormat("dd-MM-yyyy");
        datetoshow=sdf.format(date);
        year=calendar.get(Calendar.YEAR);
        monthno=calendar.get(Calendar.MONTH)+1;
        weeknumber=calendar.get(Calendar.WEEK_OF_MONTH);


        Log.e("year",String.valueOf(year));

        Log.e("month",String.valueOf(monthno));

        Log.e("week",String.valueOf(weeknumber));


        //spinner.setPrompt(String.valueOf(year));
        yearholder=(RecyclerView) findViewById(R.id.yearholder);
        monthholder=(RecyclerView) findViewById(R.id.monthholder);
        weekholder=(RecyclerView) findViewById(R.id.weekholder);
        spinner=(Spinner) findViewById(R.id.spinner);
        selecteddate=(TextView) findViewById(R.id.selecteddate);
        selecteddate.setText(datetoshow);
        yeartoshow.setText(String.valueOf(year));
        if(year==2017)
        {
            after.setVisibility(View.INVISIBLE);
        }
        else
        {
            after.setVisibility(View.VISIBLE);
        }
        before.setVisibility(View.VISIBLE);
        setyeardata();
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, spinneritems){

            @Override
            public int getCount() {

                return spinneritems.length-1;
            }

            public View getView(int position, View convertView, ViewGroup parent) {



                return setCentered(super.getView(position, convertView, parent));

            }

            public View getDropDownView(int position, View convertView,ViewGroup parent) {



                return setCentered(super.getDropDownView(position, convertView, parent));
            }

            private View setCentered(View view)
            {
                TextView textView = (TextView)view.findViewById(android.R.id.text1);
                textView.setTextColor(Color.parseColor("#1f71be"));
                textView.setGravity(Gravity.CENTER);
                return view;
            }
        };
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setSelection(3);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedinspinner=spinner.getSelectedItem().toString();
                Log.e("item selected",selectedinspinner);
                setdatasetinflipper();
                if(selectedinspinner.equals("Year"))
                {
                    monthFlipper.setDisplayedChild(0);
                    yeartoshow.setText(String.valueOf(year));
                    before.setVisibility(View.VISIBLE);
                    after.setVisibility(View.VISIBLE);
                    if(year==2017)
                    {
                        setlinegraphyearly("2017-store1.json","2017-store2.json");
                        after.setVisibility(View.INVISIBLE);
                    }
                    else
                    {
                        setlinegraphyearly("2016-store1.json","2016-store2.json");
                    }
                    Log.e("change to 0","true");
                    viewFlipper.setDisplayedChild(0);
                    spinner.setSelection(3);
                    setyeardata();
                }
                else if(selectedinspinner.equals("Month"))
                {
                    FlipperRecyclerViewAdapter.selectedmonthno= FlipperRecyclerViewAdapter.monthpos;
                    monthFlipper.setDisplayedChild(1);
                    OrgMonthAdapter.weekcount=0;
                    viewFlipper.setDisplayedChild(1);
                    spinner.setSelection(3);
                }
                else if (selectedinspinner.equals("Week"))
                {
                    monthFlipper.setDisplayedChild(2);
                    viewFlipper.setDisplayedChild(2);
                    before.setVisibility(View.INVISIBLE);
                    after.setVisibility(View.INVISIBLE);
                    spinner.setSelection(3);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Log.e("nothing","yes");
            }
        });

        setlinegraphyearly("2017-store1.json","2017-store2.json");

        before.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(year==2017)
                {
                    setlinegraphyearly("2016-store1.json","2016-store2.json");
                    after.setVisibility(View.VISIBLE);
                }
                year-=1;
                yeartoshow.setText(String.valueOf(year));
                setyeardata();
            }
        });
        after.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(year==2016)
                {
                    setlinegraphyearly("2017-store1.json","2017-store2.json");
                    after.setVisibility(View.INVISIBLE);
                }
                year+=1;
                yeartoshow.setText(String.valueOf(year));
                setyeardata();
            }
        });
    }



    private void setyeardata() {

        if(yearfirsttime==true)
        {
            month=new ArrayList<>();
            month.add("Jan");
            month.add("Feb");
            month.add("Mar");
            month.add("Apr");
            month.add("May");
            month.add("June");
            month.add("July");
            month.add("Aug");
            month.add("Sep");
            month.add("Oct");
            month.add("Nov");
            month.add("Dec");
            yearfirsttime=false;

        }
        FlipperRecyclerViewAdapter adapter=new FlipperRecyclerViewAdapter(getApplicationContext(),month);
        yearholder.setLayoutManager(new GridLayoutManager(getApplicationContext(),2));
        yearholder.setHasFixedSize(true);
        yearholder.setAdapter(adapter);
    }

    public void setdatasetinflipper() {

        if(monthfirsttime==true)
        {
            week=new ArrayList<>();
            week.add("Week 1");
            week.add("Week 2");
            week.add("Week 3");
            week.add("Week 4");
            week.add("Week 5");
            monthfirsttime=false;
        }
        if(weekfirsttime==true)
        {
            day=new ArrayList<>();
            day.add("Sun");
            day.add("Mon");
            day.add("Tue");
            day.add("Wed");
            day.add("Thu");
            day.add("Fri");
            day.add("Sat");
            dayfirsttime=false;
        }
            /*adapter=new FlipperRecyclerViewAdapter(getApplicationContext(),day);*/

        /*if(selectedinspinner.equals("Year"))
        {
            yearholder.setLayoutManager(new GridLayoutManager(getApplicationContext(),3));
        }
        else if(selectedinspinner.equals("Month"))
        {
            monthholder.setLayoutManager(new GridLayoutManager(getApplicationContext(),1));
        }
        else
        {
            weekholder.setLayoutManager(new GridLayoutManager(getApplicationContext(),2));
        }*/

        monthAdapter=new OrgMonthAdapter(getApplicationContext(),week);
        dayAdapter=new OrgDayAdapter(getApplicationContext(),day);

        monthholder.setLayoutManager(new GridLayoutManager(getApplicationContext(),1));
        weekholder.setLayoutManager(new GridLayoutManager(getApplicationContext(),2));

        monthholder.setHasFixedSize(true);
        monthholder.setAdapter(monthAdapter);
        weekholder.setHasFixedSize(true);
        //weekholder.setAdapter(dayAdapter);
    }

    private void drawlinegraph()
    {
        LineData linedata=new LineData(line_labels,alllines);
        orgchart.setData(linedata);
        orgchart.setDescription("");
        orgchart.animateY(5000);
        orgchart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        /*orgchart.getAxisLeft().setDrawLabels(false);
        orgchart.getAxisRight().setDrawLabels(false);
        orgchart.getAxisLeft().setAxisMaxValue(1.2f);
        orgchart.getAxisRight().setAxisMaxValue(1.2f);*/
        orgchart.setScaleMinima(390f, 1f);
        orgchart.setTouchEnabled(true);
        orgchart.setPinchZoom(true);

    }

    private void setvaluesLine(JSONObject json_name) {

        storeentry=new ArrayList<>();
        pos=0;
        String hour_to_add;
        line_labels.clear();
        try {
            JSONArray jsonarray=json_name.getJSONArray("smartTrayModels");
            Gson gson=new Gson();
            for(int i=0;i<jsonarray.length();i++)
            {
                JSONObject finalobject=jsonarray.getJSONObject(i);
                GsonModel gsonmodel=gson.fromJson(finalobject.toString(), GsonModel.class);
                String actualdatetime=gsonmodel.getDate();
                String[] splited = actualdatetime.split("\\s+");
                String hour[]=splited[1].split(":");
                if(Integer.parseInt(hour[0])>12)
                {
                    int actual_hour=Integer.parseInt(hour[0])%12;
                    hour_to_add=String.valueOf(actual_hour).concat(" PM");
                }
                else if(Integer.parseInt(hour[0])==12)
                {
                    hour_to_add=hour[0].concat(" PM");
                }
                else
                {
                    int actual_hour=Integer.parseInt(hour[0])%12;
                    hour_to_add=String.valueOf(actual_hour).concat(" AM");
                }

                if(Integer.parseInt(hour[0])>=8&&Integer.parseInt(hour[0])<=20) {
                    storeentry.add(new Entry(gsonmodel.getPresence(),pos));
                    line_labels.add(splited[0]+" "+hour_to_add);

                    DeviceLineModel deviceLineModel=new DeviceLineModel(pos,gsonmodel.getPresence());
                    /*//pos can be 0 to 12,so it will be greater than the arraylist size at first iteration,not after that.. after that just add the values of respective positions with the new values of that position
                    if(pos>=deviceLineModels.size())
                    {
                        deviceLineModels.add(deviceLineModel);
                        Log.e("Position,new presence",pos+","+String.valueOf(deviceLineModels.get(pos).getPresence()));
                    }
                    else
                    {
                        deviceLineModels.get(pos).setPresence(deviceLineModels.get(pos).getPresence()+gsonmodel.getPresence());
                        Log.e("Position,new presence",pos+","+String.valueOf(deviceLineModels.get(pos).getPresence()));
                    }*/
                    pos++;
                }
            }
        }catch (JSONException e)
        {
            e.printStackTrace();
        }
        Log.e("Position--->",String.valueOf(pos));
        if(color_no>=3)
        {
            color_no=color_no%3;
        }
        LineDataSet dataset = new LineDataSet(storeentry, "Store "+String.valueOf(color_no +1));
        dataset.setColor(SmartTrayColors.LineColor[0]);
        color_no++;
        dataset.setLineWidth(2);
        alllines.add(dataset);
    }

    private void setlinegraphyearly(String store1, String store2) {
        store1entry=new ArrayList<>();
        store2entry=new ArrayList<>();
        monthlabels=new ArrayList<>();
        ArrayList<LineDataSet> lines=new ArrayList<>();
        try {
            JSONObject store1json=new JSONObject(loadJSONFromAsset(store1));
            JSONArray data=store1json.getJSONArray("data");
            Log.e("data length",String.valueOf(data.length()));
            for(int i=0;i<data.length();i++)
            {
                JSONObject store1singleitem=data.getJSONObject(i);
                monthlabels.add(store1singleitem.getString("month"));
                store1entry.add(new Entry(store1singleitem.getInt("presence"),i));
            }
            Log.e("store1size",String.valueOf(store1entry.size()));
            Log.e("monthlabelsize",String.valueOf(monthlabels.size()));
            JSONObject store2json=new JSONObject(loadJSONFromAsset(store2));
            data=store2json.getJSONArray("data");
            for(int i=0;i<data.length();i++)
            {
                JSONObject store2singleitem=data.getJSONObject(i);
                store2entry.add(new Entry(store2singleitem.getInt("presence"),i));
            }
        }catch (JSONException e)
        {
            e.printStackTrace();
        }
        LineDataSet store1dataset=new LineDataSet(store1entry,store1.substring(0,11));
        store1dataset.setColor(SmartTrayColors.SmartTrayColor[0]);
        store1dataset.setDrawCubic(true);
        store1dataset.setLineWidth(3);
        LineDataSet store2dataset=new LineDataSet(store2entry,store2.substring(0,11));
        store2dataset.setColor(SmartTrayColors.SmartTrayColor[1]);
        store2dataset.setDrawCubic(true);
        store2dataset.setLineWidth(3);
        lines.add(store1dataset);
        lines.add(store2dataset);
        LineData linedata=new LineData(monthlabels,lines);
        orgchart.setData(linedata);
        //orgchart.setBackgroundColor(Color.parseColor("#bddad475"));
        orgchart.setDrawGridBackground(false);
        orgchart.setDescription("% of availibility");
        orgchart.animateY(5000);
        orgchart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        orgchart.setTouchEnabled(true);
        orgchart.setPinchZoom(true);



    }


    private String loadJSONFromAsset(String name) {
        String json = null;
        try {
            InputStream is = getApplicationContext().getAssets().open(name);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        Log.e("JSON",json);
        return json;
    }


}
