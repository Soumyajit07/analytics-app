package com.cognizant.iot.completeanalytics.StoreView.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cognizant.iot.completeanalytics.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class Annualy extends Fragment {


    public Annualy() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_annualy, container, false);
    }

}
