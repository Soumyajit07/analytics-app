package com.cognizant.iot.completeanalytics;

import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.cognizant.iot.completeanalytics.Model.SmartTrayColors;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;


/**
 * Created by Guest_User on 05/05/17.
 */

public class FlipperRecyclerViewAdapter extends RecyclerView.Adapter<FlipperRecyclerViewAdapter.FlipperItemViewHolder> {

    Context context;
    ArrayList<String> itemlist;
    public static String monthname;
    public static String todaydate=OrganizationActivity.datetoshow;      //current date no
    int monthno;                                                         //current month no
    public static int selectedmonthno;                                   //keeps track of selected month by user
    public static int monthpos;                                          //use current month no to check the visibility of next month to the user
    int datacount=0;                                                     //keeps track to make next months invisibile to the user which falls after current month
    public static String splitteddate[]=todaydate.split("-");







    public FlipperRecyclerViewAdapter(Context context, ArrayList<String> itemlist) {
        this.context = context;
        this.itemlist = itemlist;
        monthno=Integer.parseInt(splitteddate[1]);
        Log.e("monthno",String.valueOf(monthno));

    }

    @Override
    public FlipperItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.flippersingledata,parent,false);
        FlipperItemViewHolder flipperItemViewHolder=new FlipperItemViewHolder(view);
        return flipperItemViewHolder;
    }

    @Override
    public void onBindViewHolder(FlipperItemViewHolder holder, final int position) {
        datacount++;
        String item=itemlist.get(position);
        holder.buttoninflipper.setText(item);
        if(datacount>monthno&&OrganizationActivity.year==Integer.parseInt(splitteddate[2]))    //OrganizationActivity.year=selected year by user AND splitteddate[2]=current year
        {
            holder.buttoninflipper.setBackgroundColor(Color.parseColor("#BEB8B9"));
            holder.buttoninflipper.setClickable(false);
        }
        Log.e("datacount",String.valueOf(datacount));

    }

    @Override
    public int getItemCount() {
        return itemlist.size();
    }

    public class FlipperItemViewHolder extends RecyclerView.ViewHolder{

        Button buttoninflipper;
        int count=0;


        public FlipperItemViewHolder(final View itemView) {
            super(itemView);
            buttoninflipper=(Button) itemView.findViewById(R.id.buttoninflipper);
            buttoninflipper.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    OrganizationActivity.monthFlipper.setDisplayedChild(1);                 //show months to user
                    if(count==1)
                    {
                        v.setBackgroundColor(Color.parseColor("#1a4bfd"));
                        count=0;
                    }
                    else
                    {
                        count++;
                        v.setBackgroundColor(Color.parseColor("#F34C77"));
                        if(getAdapterPosition()==OrganizationActivity.monthno-1&&OrganizationActivity.year==Integer.parseInt(splitteddate[2]))
                        {
                            OrganizationActivity.monthnext.setVisibility(View.INVISIBLE);
                        }
                        else
                        {
                            OrganizationActivity.monthnext.setVisibility(View.VISIBLE);
                        }
                        OrganizationActivity.monthbefore.setVisibility(View.VISIBLE);
                        OrganizationActivity.selectedmonthinflipper.setText(buttoninflipper.getText().toString()+","+OrganizationActivity.year);
                        monthname=buttoninflipper.getText().toString();
                        monthpos=OrganizationActivity.month.indexOf(monthname);
                        selectedmonthno=monthpos;
                        //selectedmonthno=getAdapterPosition()+1;

                        Log.e("selectedmonth",String.valueOf(monthpos));
                        OrgMonthAdapter.weekcount=0;
                        OrganizationActivity.monthholder.setAdapter(OrganizationActivity.monthAdapter);
                        Log.e("done","main");
                        setmonthchart(monthname);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                OrganizationActivity.viewFlipper.showNext();
                            }
                        },300);

                    }
                }
            });
            OrganizationActivity.monthbefore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.e("selectedmonthno",String.valueOf(monthpos));
                    OrganizationActivity.monthnext.setVisibility(View.VISIBLE);

                    if(itemlist.get(monthpos).toString().equals("Jan"))
                    {
                        OrganizationActivity.year=OrganizationActivity.year-1;                                  //selected previous year with month as december
                        monthpos=11;
                        Toast.makeText(context,"Year changed to "+OrganizationActivity.year,Toast.LENGTH_LONG).show();
                    }
                    else
                    {                                                                                           //selects previous month
                        monthpos--;
                    }
                    OrganizationActivity.selectedmonthinflipper.setText(itemlist.get(monthpos)+","+OrganizationActivity.year);
                    monthname=itemlist.get(monthpos);
                    OrgMonthAdapter.weekcount=0;
                    OrganizationActivity.monthholder.setAdapter(OrganizationActivity.monthAdapter);
                    selectedmonthno--;                                                                          //works mainly in orgdayadapter
                    setmonthchart(monthname);

                }
            });
            OrganizationActivity.monthnext.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(itemlist.get(monthpos).toString().equals("Dec"))
                    {
                        OrganizationActivity.year=OrganizationActivity.year+1;
                        monthpos=0;
                        Toast.makeText(context,"Year changed to "+OrganizationActivity.year,Toast.LENGTH_LONG).show();
                    }
                    else
                    {
                        monthpos++;
                    }
                    //monthpos++;
                    Log.e("selectedmonthno",String.valueOf(monthpos));
                    OrganizationActivity.selectedmonthinflipper.setText(itemlist.get(monthpos)+","+OrganizationActivity.year);
                    if (monthpos == monthno-1) {
                        OrganizationActivity.monthnext.setVisibility(View.INVISIBLE);
                    } else
                    {
                        OrganizationActivity.monthnext.setVisibility(View.VISIBLE);
                    }
                    monthname=itemlist.get(monthpos);
                    OrgMonthAdapter.weekcount=0;
                    OrganizationActivity.monthholder.setAdapter(OrganizationActivity.monthAdapter);
                    selectedmonthno++;
                    setmonthchart(monthname);

                }
            });
        }

        private void setmonthchart(String monthname) {
            JSONObject monthdetailsstore1;
            JSONObject monthdetailsstore2;
            ArrayList<String> labels=new ArrayList<>();
            ArrayList<Entry> store1entry=new ArrayList<>();
            ArrayList<Entry> store2entry=new ArrayList<>();
            ArrayList<LineDataSet> lines=new ArrayList<>();

            try {
                if(OrganizationActivity.year==2017)
                {
                    monthdetailsstore1=new JSONObject(loadJSONFromAsset("2017-store1-weekly.json"));
                    monthdetailsstore2=new JSONObject(loadJSONFromAsset("2017-store2-weekly.json"));
                }
                else
                {
                    monthdetailsstore1=new JSONObject(loadJSONFromAsset("2016-store1-weekly.json"));
                    monthdetailsstore2=new JSONObject(loadJSONFromAsset("2016-store2-weekly.json"));
                }
                JSONArray info=monthdetailsstore1.getJSONArray("info");
                for(int i=0;i<info.length();i++)
                {
                    JSONObject monthobject=info.getJSONObject(i);
                    if(monthobject.getString("month").equals(monthname))
                    {
                        Log.e("monthname, month json",monthname+" ,"+monthobject.getString("month"));
                        JSONArray finaldata=monthobject.getJSONArray("data");
                        Log.e("finaldata size",String.valueOf(finaldata.length()));
                        for(int j=0;j<finaldata.length();j++)
                        {
                            Log.e("data taken","yes");
                            JSONObject weeklydata=finaldata.getJSONObject(j);
                            labels.add("Week "+weeklydata.getInt("week"));
                            store1entry.add(new Entry(weeklydata.getInt("presence"),j));
                        }
                        Log.e("label size,store size",String.valueOf(labels.size())+" ,"+String.valueOf(store1entry.size()));
                    }
                }
                info=monthdetailsstore2.getJSONArray("info");
                for(int i=0;i<info.length();i++)
                {
                    JSONObject monthobject=info.getJSONObject(i);
                    if(monthobject.getString("month").equals(monthname))
                    {
                        JSONArray finaldata=monthobject.getJSONArray("data");
                        for(int j=0;j<finaldata.length();j++)
                        {
                            JSONObject weeklydata=finaldata.getJSONObject(j);
                            store2entry.add(new Entry(weeklydata.getInt("presence"),j));
                        }
                    }
                }
            }catch (JSONException e)
            {
                e.printStackTrace();
            }


            LineDataSet store1dataset=new LineDataSet(store1entry,"store 1 - "+monthname+","+OrganizationActivity.year);
            store1dataset.setColor(SmartTrayColors.SmartTrayColor[0]);
            store1dataset.setDrawCubic(true);
            store1dataset.setLineWidth(3);
            LineDataSet store2dataset=new LineDataSet(store2entry,"store 2 - "+monthname+","+OrganizationActivity.year);
            store2dataset.setColor(SmartTrayColors.SmartTrayColor[1]);
            store2dataset.setDrawCubic(true);
            store2dataset.setLineWidth(3);
            lines.add(store1dataset);
            lines.add(store2dataset);
            LineData linedata=new LineData(labels,lines);
            OrganizationActivity.orgchart.setData(linedata);
            OrganizationActivity.orgchart.setDescription("% of availibility in "+monthname);
            OrganizationActivity.orgchart.animateY(5000);
            OrganizationActivity.orgchart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
            OrganizationActivity.orgchart.setTouchEnabled(true);
            OrganizationActivity.orgchart.setPinchZoom(true);

        }


        private String loadJSONFromAsset(String name) {
            String json = null;
            try {
                InputStream is = context.getAssets().open(name);
                int size = is.available();
                byte[] buffer = new byte[size];
                is.read(buffer);
                is.close();
                json = new String(buffer, "UTF-8");
            } catch (IOException ex) {
                ex.printStackTrace();
                return null;
            }
            Log.e("JSON",json);
            return json;
        }
    }
}
