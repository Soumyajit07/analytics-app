package com.cognizant.iot.completeanalytics.StoreViewOther;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cognizant.iot.completeanalytics.Model.SingleTrayModel;
import com.cognizant.iot.completeanalytics.R;
import com.github.mikephil.charting.charts.PieChart;

import java.util.ArrayList;

/**
 * Created by Guest_User on 26/04/17.
 */

public class SingleTrayAdapter extends RecyclerView.Adapter<SingleTrayAdapter.SingleTrayViewHolder>{

    ArrayList<SingleTrayModel> singleTrayModels;
    Context context;


    public SingleTrayAdapter(Context context, ArrayList<SingleTrayModel> singleTrayModels) {
        this.context = context;
        this.singleTrayModels = singleTrayModels;
    }

    @Override
    public SingleTrayViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.single_tray_analytics,parent,false);
        SingleTrayViewHolder singleTrayViewHolder=new SingleTrayViewHolder(view);
        return singleTrayViewHolder;
    }

    @Override
    public void onBindViewHolder(SingleTrayViewHolder holder, int position) {
        SingleTrayModel singleTrayModel=singleTrayModels.get(position);

        holder.tray_id.setText(String.valueOf(singleTrayModel.getTray_id()));
        holder.single_chart.setData(singleTrayModel.getPieData());
        holder.single_chart.setDescription("");
        holder.single_chart.setDrawCenterText(false);
        holder.single_chart.animateY(1000);
    }

    @Override
    public int getItemCount() {
        return singleTrayModels.size();
    }


    public class SingleTrayViewHolder extends  RecyclerView.ViewHolder{

        TextView tray_id;
        PieChart single_chart;
        ImageView single_bar_chart;

        public SingleTrayViewHolder(View itemView) {
            super(itemView);
            tray_id=(TextView) itemView.findViewById(R.id.tray_number);
            single_chart=(PieChart) itemView.findViewById(R.id.single_tray_analytics);
            single_bar_chart=(ImageView) itemView.findViewById(R.id.single_bar);

            single_bar_chart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent=new Intent(context,SingleBarActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("Tray_id",tray_id.getText().toString());
                    context.startActivity(intent);
                }
            });
        }
    }
}
