package com.cognizant.iot.completeanalytics.StoreViewOther;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.cognizant.iot.completeanalytics.MainActivity;
import com.cognizant.iot.completeanalytics.R;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class SingleBarActivity extends Activity {

    String tray_id;
    JSONObject trayJson;
    SwitchCompat switchchart;
    BarChart dailyChart;
    TextView available,unavailable,datetoshow;
    ArrayList<BarEntry> dailyentry;
    ArrayList<String> labels;
    int pos=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_bar);
        Intent intent = getIntent();
        tray_id = intent.getExtras().getString("Tray_id");
        Log.e("tray id", tray_id);
        if (Integer.parseInt(tray_id) == 1) {
            trayJson = MainActivity.tray1;
        } else if (Integer.parseInt(tray_id) == 2) {
            trayJson = MainActivity.tray2;
        } else
        {
            trayJson=MainActivity.tray3;
        }
        switchchart=(SwitchCompat)findViewById(R.id.switch_chart);
        dailyChart=(BarChart) findViewById(R.id.daily_chart);
        available=(TextView) findViewById(R.id.available);
        unavailable=(TextView) findViewById(R.id.unavailable);
        unavailable.setVisibility(View.GONE);
        available.setGravity(Gravity.END);
        datetoshow=(TextView) findViewById(R.id.date);

        datetoshow.setText(MainActivity.date);

        setValuestoChart();
        switchchart.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                pos=0;
                if(isChecked)
                {
                    available.setVisibility(View.GONE);
                    unavailable.setVisibility(View.VISIBLE);
                    unavailable.setGravity(Gravity.END);
                    datetoshow.setTextColor(Color.parseColor("#b7274b"));
                }
                else
                {
                    unavailable.setVisibility(View.GONE);
                    available.setVisibility(View.VISIBLE);
                    available.setGravity(Gravity.END);
                    datetoshow.setTextColor(Color.parseColor("#1f71be"));
                }
                setValuestoChart();

            }
        });

    }

    private void setValuestoChart() {

        String hour_to_add;
        labels=new ArrayList<>();
        dailyentry=new ArrayList<>();

        try {
            JSONArray smartTraydatas=trayJson.getJSONArray("smartTrayModels");
            for(int i=0;i<smartTraydatas.length();i++)
            {
                JSONObject finalobject=smartTraydatas.getJSONObject(i);
                if(finalobject.getString("date").contains(MainActivity.date))
                {
                    String actualdatetime=finalobject.getString("date");
                    String[] splited = actualdatetime.split("\\s+");
                    String hour[]=splited[1].split(":");
                    if(Integer.parseInt(hour[0])>=8&&Integer.parseInt(hour[0])<=20)
                    {
                        if(Integer.parseInt(hour[0])>12)
                        {
                            int actual_hour=Integer.parseInt(hour[0])%12;
                            hour_to_add=String.valueOf(actual_hour).concat(" PM");
                        }
                        else if(Integer.parseInt(hour[0])==12)
                        {
                            hour_to_add=hour[0].concat(" PM");
                        }
                        /*else if(Integer.parseInt(hour[0])==00)
                        {
                            hour_to_add="12 AM";
                        }*/
                        else
                        {
                            int actual_hour=Integer.parseInt(hour[0])%12;
                            hour_to_add=String.valueOf(actual_hour).concat(" AM");
                        }
                        labels.add(hour_to_add);
                        Log.e("date---->",hour_to_add);
                        if(switchchart.isChecked())
                        {
                            int value=finalobject.getInt("presence");
                            if(value==0)
                            {
                                value=1;
                            }
                            else
                            {
                                value=0;
                            }
                            dailyentry.add(new BarEntry(Float.valueOf(value),pos));
                        }
                        else
                        {
                            dailyentry.add(new BarEntry(Float.valueOf(finalobject.getInt("presence")),pos));
                        }
                        pos++;
                        Log.e("DATA---->",String.valueOf(finalobject.getInt("presence")));
                    }
                }
            }
        }catch (JSONException e)
        {
            e.printStackTrace();
        }

        XAxis xAxis = dailyChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        dailyChart.setData(setBarData());
        dailyChart.getAxisLeft().setDrawLabels(false);
        dailyChart.getAxisRight().setDrawLabels(false);
        dailyChart.getAxisLeft().setAxisMaxValue(1.5f);
        //dailyChart.getAxisLeft().setShowOnlyMinMax(true);

        dailyChart.setDescription("");
        //dailyChart.getXAxis().setLabelsToSkip(5);
        dailyChart.animateY(3000);
        dailyChart.getLegend().setEnabled(true);
        //dailyChart.setDescription("Shoe presence of 03-01-2017");
        //dailyChart.setDescriptionPosition(1020f,100f);
        //dailyChart.setDescriptionTextSize(12f);
        dailyChart.invalidate();
    }


    private BarData setBarData() {

        BarData bardata;
        BarDataSet dataset;
        if(switchchart.isChecked())
        {
            dataset=new BarDataSet(dailyentry,"Absent");
            Log.e("datasetabsent",dataset.toString());
            dataset.setColor(Color.rgb(238,30,102));
            dataset.setDrawValues(false);
            bardata=new BarData(labels,dataset);
        }
        else
        {
            dataset = new BarDataSet(dailyentry,"Present");
            dataset.setColor(Color.rgb(13,163,219));
            dataset.setDrawValues(false);
            bardata = new BarData(labels,dataset);
        }
        Log.e("Tag","++++");
        return bardata;
    }

}
