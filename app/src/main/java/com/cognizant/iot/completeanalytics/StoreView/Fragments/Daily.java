package com.cognizant.iot.completeanalytics.StoreView.Fragments;


import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;

import com.cognizant.iot.completeanalytics.MainActivity;
import com.cognizant.iot.completeanalytics.Model.DeviceLineModel;
import com.cognizant.iot.completeanalytics.Model.GsonModel;
import com.cognizant.iot.completeanalytics.Model.SingleTrayModel;
import com.cognizant.iot.completeanalytics.Model.SmartTrayColors;
import com.cognizant.iot.completeanalytics.R;
import com.cognizant.iot.completeanalytics.StoreViewOther.SingleTrayAdapter;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * A simple {@link Fragment} subclass.
 */
public class Daily extends Fragment {


    RecyclerView single_tray_analytics_holder;
    ArrayList<SingleTrayModel> singleTrayModels;
    ArrayList<String> labels;
    ArrayList<Entry> entries;
    ArrayList<PieData> piedatas;
    int available_count,unavailable_count;
    int tray_no=1,pos=0;
    DatePickerDialog datePickerDialog;
    private int myear,mmonth,mdate;
    String formattedmonth,formatteddate;
    ImageView date_picker_calender;
    LineChart comparison_chart;
    ArrayList<Entry> storedevicelists;
    ArrayList<LineDataSet> lines;
    ArrayList<String> line_labels;
    int color_no=0;
    ArrayList<DeviceLineModel> deviceLineModels;
    int number_of_device=3;
    TextView dateinline;


    public Daily() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_daily, container, false);
        dateinline=(TextView) view.findViewById(R.id.dateinline);
        deviceLineModels=new ArrayList<>();
        date_picker_calender=(ImageView) view.findViewById(R.id.datepicker);
        final Calendar c = Calendar.getInstance();
        c.set(2016,00,01);
        myear = c.get(Calendar.YEAR);
        mmonth = c.get(Calendar.MONTH);
        mdate = c.get(Calendar.DAY_OF_MONTH);
        singleTrayModels=new ArrayList<>();
        single_tray_analytics_holder=(RecyclerView) view.findViewById(R.id.single_tray_analytics_holder);
        setValuesPie(MainActivity.tray1);
        setValuesPie(MainActivity.tray2);
        setValuesPie(MainActivity.tray3);
        final SingleTrayAdapter adapter=new SingleTrayAdapter(view.getContext(),singleTrayModels);
        single_tray_analytics_holder.setLayoutManager(new GridLayoutManager(view.getContext(),2));
        single_tray_analytics_holder.setHasFixedSize(true);
        single_tray_analytics_holder.setAdapter(adapter);
        datePickerDialog=new DatePickerDialog(view.getContext(), AlertDialog.THEME_DEVICE_DEFAULT_LIGHT, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                int m=month+1;
                if(m>12)
                {
                    m=m%12;
                }
                if(m<10)
                {
                    formattedmonth= String.format("%02d", m);
                }
                else
                {
                    formattedmonth=String.valueOf(month);
                }
                if(dayOfMonth<10)
                {
                    formatteddate=String.format("%02d", dayOfMonth);
                }
                else
                {
                    formatteddate=String.valueOf(dayOfMonth);
                }
                String pickeddate=year+"-"+formattedmonth+"-"+formatteddate;
                MainActivity.date=pickeddate;
                tray_no=1;
                singleTrayModels.clear();
                setValuesPie(MainActivity.tray1);
                setValuesPie(MainActivity.tray2);
                setValuesPie(MainActivity.tray3);
                adapter.notifyDataSetChanged();
                setlinegraph();
            }
        },myear,mmonth,mdate);

        date_picker_calender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerDialog.show();
            }
        });
        lines=new ArrayList<>();
        comparison_chart=(LineChart) view.findViewById(R.id.store_comparison_chart);
        setlinegraph();

        return view;
    }



    private void setlinegraph() {
        lines.clear();
        deviceLineModels.clear();
        setvaluesLine(MainActivity.tray1);
        setvaluesLine(MainActivity.tray2);
        setvaluesLine(MainActivity.tray3);
        Log.e("line labels",line_labels.toString());
        Log.e("lines",lines.toString());
        setaverageline();
        LineData linedata=new LineData(line_labels,lines);
        comparison_chart.setData(linedata);
        comparison_chart.setDescription("");
        comparison_chart.animateY(5000);
        comparison_chart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        comparison_chart.getAxisLeft().setDrawLabels(false);
        comparison_chart.getAxisRight().setDrawLabels(false);
        comparison_chart.getAxisLeft().setAxisMaxValue(1.2f);
        comparison_chart.getAxisRight().setAxisMaxValue(1.2f);
        comparison_chart.setTouchEnabled(true);
        comparison_chart.setPinchZoom(true);
        dateinline.setText(MainActivity.date);

    }


    private void setValuesPie(JSONObject json_name) {
        labels=new ArrayList<>();
        entries=new ArrayList<>();
        piedatas=new ArrayList<>();
        line_labels=new ArrayList<>();
        Log.e("dateinStore",MainActivity.date);
        String hour_to_add;
        try
        {
            JSONArray jsonarray=json_name.getJSONArray("smartTrayModels");
            Gson gson=new Gson();
            available_count=0;
            unavailable_count=0;
            for(int i=0;i<jsonarray.length();i++)
            {
                JSONObject singletrayobject=jsonarray.getJSONObject(i);
                GsonModel gsonmodel=gson.fromJson(singletrayobject.toString(), GsonModel.class);
                if(gsonmodel.getDate().contains(MainActivity.date))
                {
                    String actualdatetime=gsonmodel.getDate();
                    String[] splited = actualdatetime.split("\\s+");
                    String hour[]=splited[1].split(":");
                    if(Integer.parseInt(hour[0])>=8&&Integer.parseInt(hour[0])<=20)
                    {
                        if(gsonmodel.getPresence()==1)
                        {
                            available_count++;
                        }
                        else
                        {
                            unavailable_count++;
                        }
                    }


                }
            }
        }catch (JSONException e)
        {
            e.printStackTrace();
        }

        entries.add(new Entry(available_count, 0));
        Log.e("tray no,available count",String.valueOf(tray_no)+"   "+String.valueOf(available_count));
        entries.add(new Entry(unavailable_count, 1));
        Log.e("tray no,un count",String.valueOf(tray_no)+"   "+String.valueOf(unavailable_count));
        PieDataSet dataset = new PieDataSet(entries,"");
        labels.add("Hours Available");
        labels.add("Hours Unavailable");
        PieData data = new PieData(labels, dataset);
        data.setValueTextSize(10);
        dataset.setColors(SmartTrayColors.SmartTrayColor);
        SingleTrayModel singletray=new SingleTrayModel(data,tray_no);
        singleTrayModels.add(singletray);
        Log.e("singletraymodels",singleTrayModels.toString());
        tray_no++;

    }


    private void setvaluesLine(JSONObject json_name) {

        storedevicelists=new ArrayList<>();
        pos=0;
        String hour_to_add;
        line_labels.clear();
        try {
            JSONArray jsonarray=json_name.getJSONArray("smartTrayModels");
            Gson gson=new Gson();
            for(int i=0;i<jsonarray.length();i++)
            {
                JSONObject finalobject=jsonarray.getJSONObject(i);
                GsonModel gsonmodel=gson.fromJson(finalobject.toString(), GsonModel.class);
                if(gsonmodel.getDate().contains(MainActivity.date))
                {
                    String actualdatetime=gsonmodel.getDate();
                    String[] splited = actualdatetime.split("\\s+");
                    String hour[]=splited[1].split(":");
                    if(Integer.parseInt(hour[0])>12)
                    {
                        int actual_hour=Integer.parseInt(hour[0])%12;
                        hour_to_add=String.valueOf(actual_hour).concat(" PM");
                    }
                    else if(Integer.parseInt(hour[0])==12)
                    {
                        hour_to_add=hour[0].concat(" PM");
                    }
                    else
                    {
                        int actual_hour=Integer.parseInt(hour[0])%12;
                        hour_to_add=String.valueOf(actual_hour).concat(" AM");
                    }

                    if(Integer.parseInt(hour[0])>=8&&Integer.parseInt(hour[0])<=20) {
                        storedevicelists.add(new Entry(gsonmodel.getPresence(),pos));
                        line_labels.add(hour_to_add);

                        DeviceLineModel deviceLineModel=new DeviceLineModel(pos,gsonmodel.getPresence());
                        //pos can be 0 to 12,so it will be greater than the arraylist size at first iteration,not after that.. after that just add the values of respective positions with the new values of that position
                        if(pos>=deviceLineModels.size())
                        {
                            deviceLineModels.add(deviceLineModel);
                            Log.e("Position,new presence",pos+","+String.valueOf(deviceLineModels.get(pos).getPresence()));
                        }
                        else
                        {
                            deviceLineModels.get(pos).setPresence(deviceLineModels.get(pos).getPresence()+gsonmodel.getPresence());
                            Log.e("Position,new presence",pos+","+String.valueOf(deviceLineModels.get(pos).getPresence()));
                        }
                        pos++;
                    }
                }
            }
        }catch (JSONException e)
        {
            e.printStackTrace();
        }
        Log.e("Position--->",String.valueOf(pos));
        if(color_no>=3)
        {
            color_no=color_no%3;
        }
        LineDataSet dataset = new LineDataSet(storedevicelists, "Device "+String.valueOf(color_no+1));
        dataset.setColor(SmartTrayColors.LineColor[color_no]);
        color_no++;
        /*dataset.setDrawCircles(false);
        dataset.setDrawCubic(true);*/
        dataset.setLineWidth(2);
        lines.add(dataset);
    }

    private void setaverageline() {
        storedevicelists=new ArrayList<>();
        pos=0;
        for(int i=0;i<deviceLineModels.size();i++)
        {
            storedevicelists.add(new Entry((float)deviceLineModels.get(pos).getPresence()/number_of_device,pos));
            Log.e("average,position",String.valueOf(Float.valueOf((float)deviceLineModels.get(pos).getPresence()/number_of_device)+","+pos));
            pos++;
        }
        LineDataSet dataset = new LineDataSet(storedevicelists, "Average");
        dataset.setColor(Color.rgb(232,168,21));
        dataset.setDrawCubic(true);
        dataset.setLineWidth(4);
        lines.add(dataset);

    }


}
