package com.cognizant.iot.completeanalytics.Model;

import android.graphics.Color;

/**
 * Created by Guest_User on 25/04/17.
 */

public class SmartTrayColors {

    public static final int[] SmartTrayColor={Color.rgb(13,163,219),Color.rgb(238,30,102)};
    public static final int[] LineColor = {
            Color.rgb(233, 64, 120), Color.rgb(72, 41, 225), Color.rgb(41, 225, 183),
            Color.rgb(106, 150, 31), Color.rgb(179, 100, 53),Color.rgb(221,14,183)
    };
}
