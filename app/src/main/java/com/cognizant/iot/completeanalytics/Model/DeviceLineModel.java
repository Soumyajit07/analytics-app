package com.cognizant.iot.completeanalytics.Model;

/**
 * Created by Guest_User on 28/04/17.
 */

public class DeviceLineModel {
    int pos;
    int presence;

    public DeviceLineModel(int pos, int presence) {
        this.pos = pos;
        this.presence = presence;
    }

    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }

    public int getPresence() {
        return presence;
    }

    public void setPresence(int presence) {
        this.presence = presence;
    }
}
