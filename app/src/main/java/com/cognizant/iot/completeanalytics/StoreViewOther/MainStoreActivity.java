package com.cognizant.iot.completeanalytics.StoreViewOther;

import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.os.Bundle;

import com.cognizant.iot.completeanalytics.R;
import com.cognizant.iot.completeanalytics.StoreView.Fragments.Annualy;
import com.cognizant.iot.completeanalytics.StoreView.Fragments.Daily;
import com.cognizant.iot.completeanalytics.StoreView.Fragments.Monthly;
import com.cognizant.iot.completeanalytics.StoreView.Fragments.Weekly;

public class MainStoreActivity extends FragmentActivity {

    TabLayout tabLayout;
    ViewPager viewPager;
    ViewPagerAdapter viewPagerAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_store);
        tabLayout=(TabLayout) findViewById(R.id.tablayout);
        viewPager=(ViewPager) findViewById(R.id.viewpager);
        viewPagerAdapter=new ViewPagerAdapter(getSupportFragmentManager());
        viewPagerAdapter.addFragment(new Daily(),"Daily");
        viewPagerAdapter.addFragment(new Weekly(),"Weekly");
        viewPagerAdapter.addFragment(new Monthly(),"Monthly");
        viewPagerAdapter.addFragment(new Annualy(),"Yearly");
        viewPager.setAdapter(viewPagerAdapter);
        viewPager.setOffscreenPageLimit(3);
        tabLayout.setupWithViewPager(viewPager);
    }


}
