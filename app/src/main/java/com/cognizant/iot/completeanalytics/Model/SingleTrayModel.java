package com.cognizant.iot.completeanalytics.Model;

import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;

import java.util.ArrayList;

/**
 * Created by Guest_User on 26/04/17.
 */

public class SingleTrayModel {

    int tray_id;
    PieData pieData;


    public SingleTrayModel(PieData pieData, int tray_id) {
        this.pieData = pieData;
        this.tray_id = tray_id;
    }

    public PieData getPieData() {
        return pieData;
    }

    public void setPieData(PieData pieData) {
        this.pieData = pieData;
    }

    public int getTray_id() {
        return tray_id;
    }

    public void setTray_id(int tray_id) {
        this.tray_id = tray_id;
    }
}
