package com.cognizant.iot.completeanalytics;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.BoolRes;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.cognizant.iot.completeanalytics.Model.SmartTrayColors;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by Guest_User on 09/05/17.
 */

public class OrgDayAdapter extends RecyclerView.Adapter<OrgDayAdapter.OrgDayViewHolder> {

    Context context;
    ArrayList<String> daylist;
    String selectedDay;
    int daypos;
    String dayname;
    public static String buttonttext;



    public OrgDayAdapter(Context context, ArrayList<String> daylist) {
        this.context = context;
        this.daylist = daylist;
    }

    @Override
    public OrgDayViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.flippersingledata,parent,false);
        OrgDayViewHolder OrgdayItemViewHolder=new OrgDayViewHolder(view);
        return OrgdayItemViewHolder;
    }

    @Override
    public void onBindViewHolder(OrgDayViewHolder holder, int position) {

            String item=daylist.get(position);
            holder.buttoninflipper.setText(item);

            holder.buttoninflipper.setBackgroundColor(Color.parseColor("#1a4bfd"));

            Log.e("now",holder.buttoninflipper.getText().toString());
            if(OrgMonthAdapter.weekname.contains("5")&&Integer.parseInt(finddate(holder.buttoninflipper.getText().toString()))>=1&&Integer.parseInt(finddate(holder.buttoninflipper.getText().toString()))<=5)
            {
                Log.e("started","yes");                                                     //non clickable last week of the month shows date 1 to 5
                holder.buttoninflipper.setBackgroundColor(Color.parseColor("#BEB8B9"));
                holder.buttoninflipper.setClickable(false);

            }

            if(OrgMonthAdapter.weekname.contains("1")&&Integer.parseInt(finddate(holder.buttoninflipper.getText().toString()))<=31&&Integer.parseInt(finddate(holder.buttoninflipper.getText().toString()))>=26)
            {
                Log.e("started","yes");                                                      //non clickable first week of the month shows date 26 to 31
                holder.buttoninflipper.setBackgroundColor(Color.parseColor("#BEB8B9"));
                holder.buttoninflipper.setClickable(false);

            }

            if(OrgMonthAdapter.weekpos==(OrganizationActivity.weeknumber-1)&&(FlipperRecyclerViewAdapter.selectedmonthno+1)==OrganizationActivity.monthno&&OrganizationActivity.year==2017)
            {
                Log.e("visibility","iv");                                                    //weeknext invisible if selected week,month and year is all current week,month and year
                OrganizationActivity.weeknext.setVisibility(View.INVISIBLE);
            }
            else
            {
                OrganizationActivity.weeknext.setVisibility(View.VISIBLE);
                Log.e("visibility","v");
            }

                                                                                                //selected button color change
            if(dayname!=null&&holder.buttoninflipper.getText().toString().equals(dayname)) {
                Log.e("dayname","in");
                holder.buttoninflipper.setBackgroundColor(Color.parseColor("#F34C77"));
            }







    }

    @Override
    public int getItemCount() {
        return daylist.size();
    }


    public class OrgDayViewHolder extends RecyclerView.ViewHolder{

        public Button buttoninflipper;
        int count=0;

        public OrgDayViewHolder(View itemView) {
            super(itemView);
            buttoninflipper=(Button) itemView.findViewById(R.id.buttoninflipper);
            Log.e("weekpos,weekno",String.valueOf(OrgMonthAdapter.weekpos)+","+String.valueOf(OrganizationActivity.weeknumber-1));
            Log.e("sm,mn",String.valueOf(FlipperRecyclerViewAdapter.selectedmonthno+1)+","+OrganizationActivity.monthno);
            Log.e("year",String.valueOf(OrganizationActivity.year));


            buttoninflipper.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(count==1)
                    {
                        v.setBackgroundColor(Color.parseColor("#1a4bfd"));
                        count=0;
                    }
                    else
                    {
                        count++;
                        v.setBackgroundColor(Color.parseColor("#F34C77"));
                        OrganizationActivity.yeartoshow.setText(buttoninflipper.getText().toString());
                        selectedDay=buttoninflipper.getText().toString();
                        Log.e("showdata of selectedday",selectedDay);
                        Log.e("in","yes");
                        OrganizationActivity.monthFlipper.setDisplayedChild(3);
                        OrganizationActivity.selecteddayinflipper.setText(finddate(selectedDay).concat("/"+String.valueOf(FlipperRecyclerViewAdapter.monthpos+1)+"/"+OrganizationActivity.year));
                        String datenow=OrganizationActivity.year+"/"+FlipperRecyclerViewAdapter.monthname+"/"+OrgMonthAdapter.weekname+"/"+selectedDay;
                        Log.e("date YY/MM/WW/DD",datenow);
                        OrganizationActivity.daynext.setVisibility(View.VISIBLE);
                        OrganizationActivity.daybefore.setVisibility(View.VISIBLE);
                        OrganizationActivity.spinner.setSelection(3);
                        setchart(selectedDay);
                        daypos=OrganizationActivity.day.indexOf(selectedDay);
                        dayname=OrganizationActivity.day.get(daypos);
                        Log.e("Dayname",dayname);
                        finddate(dayname);
                        buttonttext=buttoninflipper.getText().toString();
                        Log.e("buttontext",buttonttext);
                        Log.e("findhighlightable",String.valueOf(findhighlightable(buttonttext)));
                        OrganizationActivity.weekholder.setAdapter(OrganizationActivity.dayAdapter);
                        /*new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                OrganizationActivity.viewFlipper.showNext();
                            }
                        },300);*/
                    }
                }
            });

            OrganizationActivity.daynext.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //buttoninflipper.setBackgroundColor(Color.parseColor("#1a4bfd"));
                    buttonttext=dayname;
                    int id;
                    id=OrganizationActivity.day.indexOf(buttonttext);                       //take the index of current day then change it for the next day and get the name of that day
                    if(id==6)
                    {
                        id=-1;
                    }
                    buttonttext=OrganizationActivity.day.get(id+1);

                    if(findhighlightable(buttonttext)==false)                               // check if the new day is highlightable or not,if it is not then go inside
                    {
                        Log.e("i am in","yes");
                        do                                                                  //find next highlightable
                        {
                            if(daypos==6)                                                   //if day number is the end day of the week then change the day,week and month accordingly
                            {
                                daypos=0;
                                dayname=OrganizationActivity.day.get(daypos);
                                buttonttext=dayname;
                                if(OrgMonthAdapter.weekpos==4)
                                {
                                    OrgMonthAdapter.weekpos=0;
                                    FlipperRecyclerViewAdapter.selectedmonthno=FlipperRecyclerViewAdapter.selectedmonthno+1;
                                    Log.e("selectedmonthno",String.valueOf(FlipperRecyclerViewAdapter.selectedmonthno));
                                    Toast.makeText(context,"Month changed to "+OrganizationActivity.month.get(FlipperRecyclerViewAdapter.selectedmonthno),Toast.LENGTH_LONG).show();
                                }
                                else
                                {
                                    OrgMonthAdapter.weekpos=OrgMonthAdapter.weekpos+1;
                                }
                                OrgMonthAdapter.weekname=OrganizationActivity.week.get(OrgMonthAdapter.weekpos);
                                Toast.makeText(context,"Week changed to "+OrgMonthAdapter.weekname,Toast.LENGTH_LONG).show();
                            }
                            else
                            {
                                daypos++;
                                dayname=OrganizationActivity.day.get(daypos);
                                buttonttext=dayname;
                            }
                            OrganizationActivity.weekholder.setAdapter(OrganizationActivity.dayAdapter);
                        }while(findhighlightable(buttonttext)==false);
                    }
                    else
                    {                                                                       //if the next date is highlightable then select that one
                        buttoninflipper.setBackgroundColor(Color.parseColor("#1a4bfd"));
                        if(daypos==6)
                        {
                            daypos=0;
                            if(OrgMonthAdapter.weekpos==4)
                            {
                                OrgMonthAdapter.weekpos=0;
                                FlipperRecyclerViewAdapter.selectedmonthno=FlipperRecyclerViewAdapter.selectedmonthno+1;
                                Log.e("selectedmonthno",String.valueOf(FlipperRecyclerViewAdapter.selectedmonthno));
                                Toast.makeText(context,"Month changed to "+OrganizationActivity.month.get(FlipperRecyclerViewAdapter.selectedmonthno),Toast.LENGTH_LONG).show();
                            }
                                else
                            {
                                OrgMonthAdapter.weekpos=OrgMonthAdapter.weekpos+1;
                            }
                            OrgMonthAdapter.weekname=OrganizationActivity.week.get(OrgMonthAdapter.weekpos);
                            Toast.makeText(context,"Week changed to "+OrgMonthAdapter.weekname,Toast.LENGTH_LONG).show();
                        }
                        else
                        {
                            daypos++;
                            dayname=OrganizationActivity.day.get(daypos);
                            buttonttext=dayname;
                        }
                        OrganizationActivity.weekholder.setAdapter(OrganizationActivity.dayAdapter);
                    }
                    dayname=OrganizationActivity.day.get(daypos);
                    buttonttext=dayname;
                    OrganizationActivity.selecteddayinflipper.setText(finddate(dayname).concat("/"+String.valueOf(FlipperRecyclerViewAdapter.selectedmonthno+1)+"/"+OrganizationActivity.year));
                    //OrganizationActivity.weekholder.setAdapter(OrganizationActivity.dayAdapter);
                    setchart(dayname);
                }
            });

            OrganizationActivity.daybefore.setOnClickListener(new View.OnClickListener() {                      //same as the previous one
                @Override
                public void onClick(View v) {
                    //buttoninflipper.setBackgroundColor(Color.parseColor("#1a4bfd"));
                    buttonttext=dayname;
                    int id;
                    id=OrganizationActivity.day.indexOf(buttonttext);
                    if(id==0)
                    {
                        id=7;
                    }
                    buttonttext=OrganizationActivity.day.get(id-1);

                    if(findhighlightable(buttonttext)==false)
                    {
                        Log.e("i am in","yes");
                        do
                        {
                            if(daypos==0)
                            {
                                daypos=6;
                                dayname=OrganizationActivity.day.get(daypos);
                                buttonttext=dayname;
                                if(OrgMonthAdapter.weekpos==0)
                                {
                                    OrgMonthAdapter.weekpos=4;
                                    FlipperRecyclerViewAdapter.selectedmonthno=FlipperRecyclerViewAdapter.selectedmonthno-1;
                                    Log.e("selectedmonthno",String.valueOf(FlipperRecyclerViewAdapter.selectedmonthno));
                                    Toast.makeText(context,"Month changed to "+OrganizationActivity.month.get(FlipperRecyclerViewAdapter.selectedmonthno),Toast.LENGTH_LONG).show();
                                }
                                else
                                {
                                    OrgMonthAdapter.weekpos=OrgMonthAdapter.weekpos-1;
                                }
                                OrgMonthAdapter.weekname=OrganizationActivity.week.get(OrgMonthAdapter.weekpos);
                                Toast.makeText(context,"Week changed to "+OrgMonthAdapter.weekname,Toast.LENGTH_LONG).show();
                            }
                            else
                            {
                                daypos--;
                                dayname=OrganizationActivity.day.get(daypos);
                                buttonttext=dayname;
                            }
                            OrganizationActivity.weekholder.setAdapter(OrganizationActivity.dayAdapter);
                        }while(findhighlightable(buttonttext)==false);
                    }
                    else
                    {
                        buttoninflipper.setBackgroundColor(Color.parseColor("#1a4bfd"));
                        if(daypos==0)
                        {
                            daypos=6;
                            if(OrgMonthAdapter.weekpos==0)
                            {
                                OrgMonthAdapter.weekpos=4;
                                FlipperRecyclerViewAdapter.selectedmonthno=FlipperRecyclerViewAdapter.selectedmonthno-1;
                                Log.e("selectedmonthno",String.valueOf(FlipperRecyclerViewAdapter.selectedmonthno));
                                Toast.makeText(context,"Month changed to "+OrganizationActivity.month.get(FlipperRecyclerViewAdapter.selectedmonthno),Toast.LENGTH_LONG).show();
                            }
                            else
                            {
                                OrgMonthAdapter.weekpos=OrgMonthAdapter.weekpos-1;
                            }
                            OrgMonthAdapter.weekname=OrganizationActivity.week.get(OrgMonthAdapter.weekpos);
                            Toast.makeText(context,"Week changed to "+OrgMonthAdapter.weekname,Toast.LENGTH_LONG).show();
                        }
                        else
                        {
                            daypos--;
                            dayname=OrganizationActivity.day.get(daypos);
                            buttonttext=dayname;
                        }
                        OrganizationActivity.weekholder.setAdapter(OrganizationActivity.dayAdapter);
                    }
                    dayname=OrganizationActivity.day.get(daypos);
                    buttonttext=dayname;
                    OrganizationActivity.selecteddayinflipper.setText(finddate(dayname).concat("/"+String.valueOf(FlipperRecyclerViewAdapter.selectedmonthno+1)+"/"+OrganizationActivity.year));
                    //OrganizationActivity.weekholder.setAdapter(OrganizationActivity.dayAdapter);
                    setchart(dayname);
                }
            });

        }
    }

    private void setchart(String selectedDay) {                                                         //set chart of the selected day
        JSONObject dailystore1=new JSONObject();
        JSONObject dailystore2=new JSONObject();
        ArrayList<String> labels=new ArrayList<>();
        ArrayList<Entry> store1entry=new ArrayList<>();
        ArrayList<Entry> store2entry=new ArrayList<>();
        ArrayList<LineDataSet> lines=new ArrayList<>();
        try {
            switch (selectedDay)
            {
                case "Sun":dailystore1=new JSONObject(loadJSONFromAsset("Sundaystore1.json"));
                    dailystore2=new JSONObject(loadJSONFromAsset("Sundaystore2.json"));
                    break;
                case "Mon":dailystore1=new JSONObject(loadJSONFromAsset("Mondaystore1.json"));
                    dailystore2=new JSONObject(loadJSONFromAsset("Mondaystore2.json"));
                    break;
                case "Tue":dailystore1=new JSONObject(loadJSONFromAsset("Tuesdaystore1.json"));
                    dailystore2=new JSONObject(loadJSONFromAsset("Tuesdaystore2.json"));
                    break;
                case "Wed":dailystore1=new JSONObject(loadJSONFromAsset("Wednesdaystore1.json"));
                    dailystore2=new JSONObject(loadJSONFromAsset("Wednesdaystore2.json"));
                    break;
                case "Thu":dailystore1=new JSONObject(loadJSONFromAsset("Thursdaystore1.json"));
                    dailystore2=new JSONObject(loadJSONFromAsset("Thursdaystore2.json"));
                    break;
                case "Fri":dailystore1=new JSONObject(loadJSONFromAsset("Fridaystore1.json"));
                    dailystore2=new JSONObject(loadJSONFromAsset("Fridaystore2.json"));
                    break;
                case "Sat":dailystore1=new JSONObject(loadJSONFromAsset("Saturdaystore1.json"));
                    dailystore2=new JSONObject(loadJSONFromAsset("Saturdaystore2.json"));
                    break;
            }
            JSONArray datastore1=dailystore1.getJSONArray("data");
            JSONArray datastore2=dailystore2.getJSONArray("data");
            for(int i=0;i<datastore1.length();i++)
            {
                JSONObject singlehourstore1=datastore1.getJSONObject(i);
                JSONObject singlehourstore2=datastore2.getJSONObject(i);
                labels.add(singlehourstore1.getString("time"));
                store1entry.add(new Entry(singlehourstore1.getInt("presence"),i));
                store2entry.add(new Entry(singlehourstore2.getInt("presence"),i));
            }
        }catch (JSONException e)
        {
            e.printStackTrace();
        }
        LineDataSet store1dataset=new LineDataSet(store1entry,"store 1 - "+selectedDay);
        store1dataset.setColor(SmartTrayColors.SmartTrayColor[0]);
        store1dataset.setDrawCubic(true);
        store1dataset.setLineWidth(3);
        LineDataSet store2dataset=new LineDataSet(store2entry,"store 2 - "+selectedDay);
        store2dataset.setColor(SmartTrayColors.SmartTrayColor[1]);
        store2dataset.setDrawCubic(true);
        store2dataset.setLineWidth(3);
        lines.add(store1dataset);
        lines.add(store2dataset);
        LineData linedata=new LineData(labels,lines);
        OrganizationActivity.orgchart.setData(linedata);
        OrganizationActivity.orgchart.setDescription("% of availibility in "+selectedDay);
        OrganizationActivity.orgchart.animateY(5000);
        OrganizationActivity.orgchart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        OrganizationActivity.orgchart.setTouchEnabled(true);
        OrganizationActivity.orgchart.setPinchZoom(true);
    }


    private String loadJSONFromAsset(String name) {
        String json = null;
        try {
            InputStream is = context.getAssets().open(name);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        Log.e("JSON",json);
        return json;
    }

    private String finddate(String Dayname)                                                             //returns the date(only day part) after getting the the dayname as parameter
    {
        SimpleDateFormat sdf = new SimpleDateFormat("MM dd yyyy");
        SimpleDateFormat dateformat=new SimpleDateFormat("dd");
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR,OrganizationActivity.year);
        cal.set(Calendar.MONTH,FlipperRecyclerViewAdapter.selectedmonthno);
        Log.e("monthpos",String.valueOf(FlipperRecyclerViewAdapter.selectedmonthno));
        cal.set(Calendar.WEEK_OF_MONTH, OrgMonthAdapter.weekpos+1);
        Log.e("weekpos",String.valueOf(OrgMonthAdapter.weekpos+1));
        Log.e("daypos",String.valueOf(daypos));
        Log.e("selecteddate",Dayname);
        switch (Dayname)
        {
            case "Sun":cal.set(Calendar.DAY_OF_WEEK,Calendar.SUNDAY);
                break;
            case "Mon":cal.set(Calendar.DAY_OF_WEEK,Calendar.MONDAY);
                break;
            case "Tue":cal.set(Calendar.DAY_OF_WEEK,Calendar.TUESDAY);
                break;
            case "Wed":cal.set(Calendar.DAY_OF_WEEK,Calendar.WEDNESDAY);
                break;
            case "Thu":cal.set(Calendar.DAY_OF_WEEK,Calendar.THURSDAY);
                break;
            case "Fri":cal.set(Calendar.DAY_OF_WEEK,Calendar.FRIDAY);
                break;
            case "Sat":cal.set(Calendar.DAY_OF_WEEK,Calendar.SATURDAY);
                break;
        }
        Log.e("dateday",dateformat.format(cal.getTime()));
        Log.e("date",sdf.format(cal.getTime()));
        return dateformat.format(cal.getTime());
    }

    public Boolean findhighlightable(String Dayname)                                                    //returns whether a day of a week and month and year is highlightable or not
    {
        SimpleDateFormat sdf = new SimpleDateFormat("MM dd yyyy");
        SimpleDateFormat dateformat=new SimpleDateFormat("dd");
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR,OrganizationActivity.year);
        cal.set(Calendar.MONTH,FlipperRecyclerViewAdapter.selectedmonthno);
        //Log.e("monthpos",String.valueOf(FlipperRecyclerViewAdapter.monthpos));
        cal.set(Calendar.WEEK_OF_MONTH, OrgMonthAdapter.weekpos+1);
        Log.e("weekpos",String.valueOf(OrgMonthAdapter.weekpos+1));
        Log.e("daypos",String.valueOf(daypos));
        Log.e("selecteddate",Dayname);
        switch (Dayname)
        {
            case "Sun":cal.set(Calendar.DAY_OF_WEEK,Calendar.SUNDAY);
                break;
            case "Mon":cal.set(Calendar.DAY_OF_WEEK,Calendar.MONDAY);
                break;
            case "Tue":cal.set(Calendar.DAY_OF_WEEK,Calendar.TUESDAY);
                break;
            case "Wed":cal.set(Calendar.DAY_OF_WEEK,Calendar.WEDNESDAY);
                break;
            case "Thu":cal.set(Calendar.DAY_OF_WEEK,Calendar.THURSDAY);
                break;
            case "Fri":cal.set(Calendar.DAY_OF_WEEK,Calendar.FRIDAY);
                break;
            case "Sat":cal.set(Calendar.DAY_OF_WEEK,Calendar.SATURDAY);
                break;
        }
        Log.e("dateday",dateformat.format(cal.getTime()));
        Log.e("date",sdf.format(cal.getTime()));
        if((OrgMonthAdapter.weekname.contains("5")&&Integer.parseInt(finddate(buttonttext))>=1&&Integer.parseInt(finddate(buttonttext))<=5)||(OrgMonthAdapter.weekname.contains("1")&&Integer.parseInt(finddate(buttonttext))<=31&&Integer.parseInt(finddate(buttonttext))>=26))
        {
            return false;
        }
        return true;
    }
}
