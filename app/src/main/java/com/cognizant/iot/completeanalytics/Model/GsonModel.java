package com.cognizant.iot.completeanalytics.Model;

/**
 * Created by Guest_User on 26/04/17.
 */

public class GsonModel {

    String date;
    int presence;

    public GsonModel(String date, int presence) {
        this.date = date;
        this.presence = presence;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getPresence() {
        return presence;
    }

    public void setPresence(int presence) {
        this.presence = presence;
    }
}
