package com.cognizant.iot.completeanalytics;

import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.cognizant.iot.completeanalytics.Model.SmartTrayColors;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Guest_User on 09/05/17.
 */

public class OrgMonthAdapter extends RecyclerView.Adapter<OrgMonthAdapter.OrgMonthViewHolder> {

    Context context;
    ArrayList<String> months;
    String day=FlipperRecyclerViewAdapter.splitteddate[0];              //current day
    int weekno=Integer.parseInt(day)/7;                                 //current week
    public static int weekcount;
    ArrayList<String> labels;
    ArrayList<Entry> store1entry;
    ArrayList<Entry> store2entry;
    ArrayList<LineDataSet> lines;
    public static String weekname;
    public static int weekpos;

    public OrgMonthAdapter(Context context, ArrayList<String> months) {
        weekcount=0;
        this.context = context;
        this.months = months;

    }

    @Override
    public OrgMonthViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.flippersingledata,parent,false);
        OrgMonthViewHolder OrgmonthItemViewHolder=new OrgMonthViewHolder(view);
        return OrgmonthItemViewHolder;
    }

    @Override
    public void onBindViewHolder(OrgMonthViewHolder holder, int position) {

        String item=months.get(position);
        holder.buttoninflipper.setText(item);
        /*if(OrganizationActivity.month.get(FlipperRecyclerViewAdapter.selectedmonthno).equals("Feb"))
        {
            if(holder.buttoninflipper.getText().toString().contains("5"))
            {
                Log.e("o","yeah");
                holder.buttoninflipper.setBackgroundColor(Color.parseColor("#BEB8B9"));
                holder.buttoninflipper.setClickable(false);
            }
        }*/
        Log.e("selectedmonth,curmonth",String.valueOf(FlipperRecyclerViewAdapter.monthpos)+","+String.valueOf(OrganizationActivity.monthno));
        Log.e("weekcount,weekno",String.valueOf(weekcount)+","+String.valueOf(weekno));
        if(weekcount>weekno)                                                                //next weeks of the current month are non clickable
        {
            if(OrganizationActivity.year==Integer.parseInt(FlipperRecyclerViewAdapter.splitteddate[2])&&FlipperRecyclerViewAdapter.monthpos+1==OrganizationActivity.monthno)
            {
                holder.buttoninflipper.setBackgroundColor(Color.parseColor("#BEB8B9"));
                holder.buttoninflipper.setClickable(false);
            }
        }
        weekcount++;
    }

    @Override
    public int getItemCount() {
        return months.size();
    }


    public class OrgMonthViewHolder extends RecyclerView.ViewHolder{

        Button buttoninflipper;
        int count=0;

        public OrgMonthViewHolder(View itemView) {
            super(itemView);
            buttoninflipper=(Button) itemView.findViewById(R.id.buttoninflipper);
            buttoninflipper.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    OrganizationActivity.monthFlipper.setDisplayedChild(2);                 //show days
                    if(count==1)
                    {
                        v.setBackgroundColor(Color.parseColor("#1a4bfd"));
                        count=0;
                    }
                    else
                    {
                        count++;
                        v.setBackgroundColor(Color.parseColor("#F34C77"));
                        OrganizationActivity.yeartoshow.setText(buttoninflipper.getText().toString());
                        weekname=buttoninflipper.getText().toString();
                        OrganizationActivity.weekno.setText(weekname+","+FlipperRecyclerViewAdapter.monthname+","+OrganizationActivity.year);
                        setweekchart(weekname);
                        weekpos=OrganizationActivity.week.indexOf(weekname);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                OrganizationActivity.weekholder.setAdapter(OrganizationActivity.dayAdapter);
                                OrganizationActivity.viewFlipper.showNext();
                            }
                        },300);
                    }
                }
            });
            OrganizationActivity.weekbefore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(weekpos==0)                                                              //week goes to final week of last month if week is first week or else goes to previous week
                    {
                        weekpos=4;
                        if(FlipperRecyclerViewAdapter.monthpos==0)
                        {
                            FlipperRecyclerViewAdapter.monthpos=11;
                            OrganizationActivity.year=OrganizationActivity.year-1;
                            Toast.makeText(context,"Year changed to "+OrganizationActivity.year,Toast.LENGTH_LONG).show();
                        }
                        else
                        {
                            FlipperRecyclerViewAdapter.monthpos=FlipperRecyclerViewAdapter.monthpos-1;
                        }
                        FlipperRecyclerViewAdapter.monthname=OrganizationActivity.month.get(FlipperRecyclerViewAdapter.monthpos);
                        Toast.makeText(context,"Month changed to "+FlipperRecyclerViewAdapter.monthname,Toast.LENGTH_LONG).show();
                    }
                    else
                    {
                        weekpos--;
                    }
                    weekname=OrganizationActivity.week.get(weekpos);
                    OrganizationActivity.weekno.setText(weekname+","+FlipperRecyclerViewAdapter.monthname+","+OrganizationActivity.year);
                    OrganizationActivity.weekholder.setAdapter(OrganizationActivity.dayAdapter);
                    setweekchart(weekname);
                }
            });
            OrganizationActivity.weeknext.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {                           //week goes to first week of next month if week is last week or else goes to next week
                    if(weekpos==4)
                    {
                        weekpos=0;
                        if(FlipperRecyclerViewAdapter.monthpos==11)
                        {
                            FlipperRecyclerViewAdapter.monthpos=0;
                            OrganizationActivity.year=OrganizationActivity.year+1;
                            Toast.makeText(context,"Year changed to "+OrganizationActivity.year,Toast.LENGTH_LONG).show();
                        }
                        else
                        {
                            FlipperRecyclerViewAdapter.monthpos=FlipperRecyclerViewAdapter.monthpos+1;
                        }
                        FlipperRecyclerViewAdapter.monthname=OrganizationActivity.month.get(FlipperRecyclerViewAdapter.monthpos);
                        Toast.makeText(context,"Month changed to "+FlipperRecyclerViewAdapter.monthname,Toast.LENGTH_LONG).show();
                    }
                    else
                    {
                        weekpos++;
                    }
                    weekname=OrganizationActivity.week.get(weekpos);
                    OrganizationActivity.weekno.setText(weekname+","+FlipperRecyclerViewAdapter.monthname+","+OrganizationActivity.year);
                    OrganizationActivity.weekholder.setAdapter(OrganizationActivity.dayAdapter);
                    setweekchart(weekname);
                }
            });
        }
    }

    private void setweekchart(String weekname) {

        labels=new ArrayList<>();
        store1entry=new ArrayList<>();
        store2entry=new ArrayList<>();
        lines=new ArrayList<>();
        JSONObject weekobject1=new JSONObject();
        JSONObject weekobject2=new JSONObject();
        int weekjsonnumberstore1=findrandom();
        int weekjsonnumberstore2=findrandom();
        while (weekjsonnumberstore2==weekjsonnumberstore1)
        {
            weekjsonnumberstore2=findrandom();
        }

        try {
            switch (weekjsonnumberstore1)
            {
                case 1: weekobject1=new JSONObject(loadJSONFromAsset("weekdrilldown1.json"));
                    break;
                case 2:weekobject1=new JSONObject(loadJSONFromAsset("weekdrilldown2.json"));
                    break;
                case 3:weekobject1=new JSONObject(loadJSONFromAsset("weekdrilldown3.json"));
                    break;
                case 4:weekobject1=new JSONObject(loadJSONFromAsset("weekdrilldown4.json"));
                    break;
                case 5:weekobject1=new JSONObject(loadJSONFromAsset("weekdrilldown1.json"));
                    break;
                default:break;
            }
            switch (weekjsonnumberstore2)
            {
                case 1: weekobject2=new JSONObject(loadJSONFromAsset("weekdrilldown1.json"));
                    break;
                case 2:weekobject2=new JSONObject(loadJSONFromAsset("weekdrilldown2.json"));
                    break;
                case 3:weekobject2=new JSONObject(loadJSONFromAsset("weekdrilldown3.json"));
                    break;
                case 4:weekobject2=new JSONObject(loadJSONFromAsset("weekdrilldown4.json"));
                    break;
                case 5:weekobject2=new JSONObject(loadJSONFromAsset("weekdrilldown3.json"));
                    break;
                default:break;
            }

            JSONArray weekarray=weekobject1.getJSONArray("weekModels");
            for(int i=0;i<weekarray.length();i++)
            {
                JSONObject weeksingleobject=weekarray.getJSONObject(i);
                if(weeksingleobject.getInt("week")==weekjsonnumberstore1)
                {
                    JSONArray finalweekarray=weeksingleobject.getJSONArray("data");
                    for (int j=0;j<finalweekarray.length();j++)
                    {
                        JSONObject finalweekobject=finalweekarray.getJSONObject(j);
                        labels.add(finalweekobject.getString("day"));
                        store1entry.add(new Entry(finalweekobject.getInt("presence"),j));
                    }
                }
            }

            weekarray=weekobject2.getJSONArray("weekModels");
            for(int i=0;i<weekarray.length();i++)
            {
                JSONObject weeksingleobject=weekarray.getJSONObject(i);
                if(weeksingleobject.getInt("week")==weekjsonnumberstore1)
                {
                    JSONArray finalweekarray=weeksingleobject.getJSONArray("data");
                    for (int j=0;j<finalweekarray.length();j++)
                    {
                        JSONObject finalweekobject=finalweekarray.getJSONObject(j);
                        store2entry.add(new Entry(finalweekobject.getInt("presence"),j));
                    }
                }
            }
        }catch (JSONException e)
        {
            e.printStackTrace();
        }

        setthechart();
    }

    private void setthechart() {

        LineDataSet store1dataset=new LineDataSet(store1entry,"store 1 - "+weekname+","+FlipperRecyclerViewAdapter.monthname);
        store1dataset.setColor(SmartTrayColors.SmartTrayColor[0]);
        store1dataset.setDrawCubic(true);
        store1dataset.setLineWidth(3);
        LineDataSet store2dataset=new LineDataSet(store2entry,"store 2 - "+weekname+","+FlipperRecyclerViewAdapter.monthname);
        store2dataset.setColor(SmartTrayColors.SmartTrayColor[1]);
        store2dataset.setDrawCubic(true);
        store2dataset.setLineWidth(3);
        lines.add(store1dataset);
        lines.add(store2dataset);
        LineData linedata=new LineData(labels,lines);
        OrganizationActivity.orgchart.setData(linedata);
        OrganizationActivity.orgchart.setDescription("% of availibility in "+weekname);
        OrganizationActivity.orgchart.animateY(5000);
        OrganizationActivity.orgchart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        OrganizationActivity.orgchart.setTouchEnabled(true);
        OrganizationActivity.orgchart.setPinchZoom(true);

    }

    private int findrandom()
    {
        Random random=new Random();
        int max=4,min=1;
        return random.nextInt(max - min + 1) + min;
    }

    private String loadJSONFromAsset(String name) {
        String json = null;
        try {
            InputStream is = context.getAssets().open(name);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        Log.e("JSON",json);
        return json;
    }
}
